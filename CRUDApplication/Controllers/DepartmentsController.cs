﻿using CRUDLibrary3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRUDApplication.Controllers
{
    public class DepartmentsController : ApiController
    {
        MyOrgEntities OE = new MyOrgEntities();

        public IQueryable<tbl_Departments> Get()
        {
            return OE.tbl_Departments;
        }
    }
}
