﻿using CRUDLibrary3;
using System.Collections.Generic;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CRUDApp2.Controllers
{
    public class DepartmentsController : ApiController
    {
        MyOrgEntities OE = new MyOrgEntities();

        //...api/Departments
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse<IEnumerable<tbl_Departments>>
                (HttpStatusCode.OK, OE.tbl_Departments);
        }



        //...api/Departments/1
        public HttpResponseMessage Get(int id)
        {
            tbl_Departments getById = OE.tbl_Departments.Find(id);
            if (getById != null)
                return Request.CreateResponse(HttpStatusCode.OK, getById);
            else
                return Request.CreateResponse(HttpStatusCode.NotFound, "Hey, may be you entered an invalid id!");
        }



        //...api/Departments
        public HttpResponseMessage Put(int id, tbl_Departments department)
        {
            tbl_Departments dept = OE.tbl_Departments.Find(id);
            if (dept == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, "Dept does not exist");
            }

            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            else
            {

                OE.Entry(department).State = EntityState.Modified;
                OE.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, department);
            }
            
        }



        //...api/Departments/id
        public HttpResponseMessage Delete(int id)
        {
            tbl_Departments department = OE.tbl_Departments.Find(id);
            if(department != null)
            {
                OE.tbl_Departments.Remove(department);
                OE.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, department);
            }

            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Record does not exist");
            }

       
        }



        //this is a delete method
        //...api/Departments
        public HttpResponseMessage Post(tbl_Departments department)
        {
            if(!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            else
            {
                OE.tbl_Departments.Add(department);
                OE.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.Created, department);

            }
            
        }
    }
}
